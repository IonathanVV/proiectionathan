package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.*;

public class WishlistPage extends Page{
    private final SelenideElement productName = $(".card-link");
    private final SelenideElement removeFromWishlistButton = $(".btn-link svg[data-icon='heart-broken']");

    public String getProductName(){
        return productName.getText();
    }
    public void clickOnRemoveButton(){
        removeFromWishlistButton.click();
    }
}
