package org.fasttrackit;

import java.util.List;

public class Main {
  public static void main(String[] args) {
    // Open page and login.
    DemoShopPage page = new DemoShopPage();
    page.openDemoShopApp();
    String greetingsMsg = page.getHeader().getGreetingsMsg();
    System.out.println("Validate that the greetings msg is: " + greetingsMsg);
    page.getHeader().clickOnTheLoginButton();
    LoginModal modal = new LoginModal();
    modal.fillInUsername("beetle");
    modal.fillInPassword("choochoo");
    modal.clickSubmitButton();
    Header header = new Header();
    String greetingsMessage = header.getGreetingsMsg();
    System.out.println("Validate that the greetings msg is: " + greetingsMessage);

    System.out.println("-------------------------------------------------");

    // Add a product to Basket
    CartPage cartDetails = new CartPage();

    Product metalMouseProduct = new Product("9", "Practical Metal Mouse","9.99");
    page.getHeader().clickOnTheCartIcon();
    List<Product> productsInCart = cartDetails.getProductsInCart();
    System.out.println("Verify that the product practical metal mouse is in the cart: " + productsInCart.get(0).getName());
    System.out.println("Verify that the product practical metal mouse price is 9.99: " + cartDetails.getProductsInCart().get(0).getPrice());
    System.out.println("Verify that the total price in cart is 9.99: " + cartDetails.getTotalCartCostBasedOnProducts());

    // Add 2 products to cart
    page.refresh();
    Product metalMouse2ndProduct = new Product("9", "Practical Metal Mouse","9.99");
    metalMouse2ndProduct.addToCart();
    Product softPizza = new Product("7", "Gorgeous Soft Pizza","19.99");
    CartPage cart2ProductsDetails = new CartPage();
    page.getHeader().clickOnTheCartIcon();
    System.out.println("Products in cart: " + cart2ProductsDetails.getNumberOfDistinctProducts());
  }
}
