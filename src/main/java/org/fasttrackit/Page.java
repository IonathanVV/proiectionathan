package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class Page {
    private final SelenideElement searchField = $("#input-search");
    private final SelenideElement searchButton = $("button[class='btn btn-light btn-sm']");
    private final SelenideElement searchResult = $(".card-link");
    private final SelenideElement sortDropDown = $(".sort-products-select.form-control.form-control-sm");
    private final SelenideElement azSort = $(".card-img[alt=\"Awesome Granite Chips product photo [100%x150]\"]");
    private final SelenideElement zaSort = $(".card-img[alt=\"Refined Frozen Mouse product photo [100%x150]\"]");
    private final SelenideElement lohiSort =$("body > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > p:nth-child(1) > span:nth-child(1)");
    private final SelenideElement hiloSort =$("body > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > p:nth-child(1) > span:nth-child(1)");
    public Page() {
        System.out.println("Opened a new page.");
    }

    public void openDemoShopApp() {
        String demoShopUrl = "https://fasttrackit-test.netlify.app/#/";
        System.out.println("Opening: " + demoShopUrl);
        open(demoShopUrl);
    }

    public void refresh() {
        System.out.println("Refreshing the page.");
    }
    public void fillInSearchField(String productName){
        searchField.sendKeys(productName);
    }
    public void clickOnSearchButton(){
        searchButton.click();
    }
    public String getSearchResult(){
        return searchResult.getText();
    }

    public void selectOptionFromSortDropDown(String option) {
        sortDropDown.selectOptionByValue(option);
    }
    public String getAZSortResult(){
        return azSort.getAttribute("alt");
    }
    public String getZASortResult() {
        return zaSort.getAttribute("alt");
    }
    public String getLoHiSortResult(){
        return hiloSort.getText();
    }

    public String getHiLoSortResults(){
        return lohiSort.getText();
    }

}
