package org.fastrackit.addToWishlist;

import io.qameta.allure.Feature;
import org.fastrackit.dataprovider.User;
import org.fastrackit.dataprovider.UserDataProvider;
import org.fasttrackit.DemoShopPage;
import org.fasttrackit.Footer;
import org.fasttrackit.LoginModal;
import org.fasttrackit.Product;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("Add to wishlist")
public class LoggedUserAddToWishlistTest {

    DemoShopPage page;

    @BeforeMethod
    public void setup() {
        this.page = new DemoShopPage();
        page.openDemoShopApp();
    }

    @AfterMethod
    public void cleanup() {
        System.out.println("Cleaning up after the test.");
        Footer footer = new Footer();
        footer.resetPage();
    }
    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider")
    public void loggedUserAddProductToWishlist(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        Product metalMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMouse.addToWishlist();
        boolean areProductsAdded = page.getHeader().areAddedProductsInWishlist();
        assertTrue(areProductsAdded, "Wishlist badge is displayed when products are added to wishlist.");
        String numberOfProductsInWishlist = page.getHeader().getNumberOfProductsInWishlist();
        assertEquals(numberOfProductsInWishlist, "1", "Logged in user and ads 1 product to wishlist.");

    }
    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider")
    public void loggedUserAddMultipleProductsToWishlist(User user){
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        Product metalMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMouse.addToWishlist();
        Product graniteChips = new Product("1", "Awesome Granite Chips", "15.99");
        graniteChips.addToWishlist();
        Product metalChair = new Product("3", "Awesome Metal Chair", "15.99");
        metalChair.addToWishlist();
        boolean areProductsAdded = page.getHeader().areAddedProductsInWishlist();
        assertTrue(areProductsAdded, "Wishlist badge is displayed when products are added to wishlist.");
        String numberOfProductsInWishlist = page.getHeader().getNumberOfProductsInWishlist();
        assertEquals(numberOfProductsInWishlist, "3", "Logged in user and ads 3 product to wishlist.");

    }
}
