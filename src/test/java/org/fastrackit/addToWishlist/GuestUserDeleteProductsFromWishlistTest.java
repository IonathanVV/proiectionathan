package org.fastrackit.addToWishlist;

import io.qameta.allure.Feature;
import org.fasttrackit.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
@Feature("Delete from wishlist")
public class GuestUserDeleteProductsFromWishlistTest {
    DemoShopPage page;

    @BeforeMethod
    public void setup() {
        this.page = new DemoShopPage();
        page.openDemoShopApp();
    }

    @AfterMethod
    public void cleanup() {
        System.out.println("Cleaning up after the test.");
        Footer footer = new Footer();
        footer.resetPage();
    }
    @Test
    public void GuestUserDeleteProductToWishlist() {
        WishlistPage wishlistPage = new WishlistPage();

        Product metalMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMouse.addToWishlist();
        page.getHeader().clickOnTheWishlistIcon();
        assertEquals(wishlistPage.getProductName(),"Practical Metal Mouse");
        wishlistPage.clickOnRemoveButton();
        boolean areProductsAdded = page.getHeader().areAddedProductsInWishlist();
        assertFalse(areProductsAdded);

    }
}
