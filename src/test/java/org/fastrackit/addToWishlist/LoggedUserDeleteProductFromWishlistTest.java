package org.fastrackit.addToWishlist;

import io.qameta.allure.Feature;
import org.fastrackit.dataprovider.User;
import org.fastrackit.dataprovider.UserDataProvider;
import org.fasttrackit.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

@Feature("Delete from wishlist")
public class LoggedUserDeleteProductFromWishlistTest {
    DemoShopPage page;

    @BeforeMethod
    public void setup() {
        this.page = new DemoShopPage();
        page.openDemoShopApp();
    }

    @AfterMethod
    public void cleanup() {
        System.out.println("Cleaning up after the test.");
        Footer footer = new Footer();
        footer.resetPage();
    }
    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider")
    public void loggedUserDeleteProductToWishlist(User user) {
        WishlistPage wishlistPage = new WishlistPage();
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        Product metalMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMouse.addToWishlist();
        page.getHeader().clickOnTheWishlistIcon();
        assertEquals(wishlistPage.getProductName(),"Practical Metal Mouse");
        wishlistPage.clickOnRemoveButton();
        boolean areProductsAdded = page.getHeader().areAddedProductsInWishlist();
        assertFalse(areProductsAdded);
    }
}
