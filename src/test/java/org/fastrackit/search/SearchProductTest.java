package org.fastrackit.search;

import io.qameta.allure.Feature;
import org.fasttrackit.DemoShopPage;
import org.fasttrackit.Footer;
import org.fasttrackit.Page;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Feature("Search for a product")
public class SearchProductTest {
    DemoShopPage page;

    @BeforeMethod
    public void setup() {
        this.page = new DemoShopPage();
        page.openDemoShopApp();
    }

    @AfterMethod
    public void cleanup() {
        System.out.println("Cleaning up after the test.");
        Footer footer = new Footer();
        footer.resetPage();
    }
    @Test
    public void searchForAProduct(){
        Page page1 = new Page();

        page1.fillInSearchField("metal mouse");
        page1.clickOnSearchButton();
        assertEquals(page1.getSearchResult(),"Practical Metal Mouse");

    }
}
