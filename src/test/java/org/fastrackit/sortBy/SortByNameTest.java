package org.fastrackit.sortBy;

import io.qameta.allure.Feature;
import org.fasttrackit.DemoShopPage;
import org.fasttrackit.Footer;
import org.fasttrackit.Page;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Feature("Sort products by name")
public class SortByNameTest {
    DemoShopPage page;

    @BeforeMethod
    public void setup() {
        this.page = new DemoShopPage();
        page.openDemoShopApp();
    }

    @AfterMethod
    public void cleanup() {
        System.out.println("Cleaning up after the test.");
        Footer footer = new Footer();
        footer.resetPage();
    }
    @Test
    public void sortProductsByNameAZ(){
        Page page1 = new Page();
        page1.selectOptionFromSortDropDown("az");
        assertEquals(page1.getAZSortResult(),"Awesome Granite Chips product photo [100%x150]");

    }
    @Test
    public void sortProductsByNameZA(){
        Page page1 = new Page();
        page1.selectOptionFromSortDropDown("za");
        assertEquals(page1.getZASortResult(),"Refined Frozen Mouse product photo [100%x150]");

    }
}
