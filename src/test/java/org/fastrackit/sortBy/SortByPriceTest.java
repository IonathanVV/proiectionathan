package org.fastrackit.sortBy;

import io.qameta.allure.Feature;
import org.fasttrackit.DemoShopPage;
import org.fasttrackit.Footer;
import org.fasttrackit.Page;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Feature("Sort products by price")
public class SortByPriceTest {
    DemoShopPage page;

    @BeforeMethod
    public void setup() {
        this.page = new DemoShopPage();
        page.openDemoShopApp();
    }

    @AfterMethod
    public void cleanup() {
        System.out.println("Cleaning up after the test.");
        Footer footer = new Footer();
        footer.resetPage();
    }
    @Test
    public void sortProductByPriceLoHi(){
    Page page1 =new Page();
    page1.selectOptionFromSortDropDown("lohi");
    assertEquals(page1.getLoHiSortResult(), "1.99");

    }


    @Test
    public void sortProductByPriceHilo(){
        Page page1 = new Page();
        page1.selectOptionFromSortDropDown("hilo");
        assertEquals(page1.getHiLoSortResults(), "29.99");

    }
}

